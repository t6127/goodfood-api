<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AddressRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AddressRepository::class)]
#[ApiResource]
class Address
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private string $addressLine;

    #[ORM\Column]
    private string $addressLine2 = '';

    #[ORM\Column]
    private string $addressLine3 = '';

    #[ORM\Column]
    private string $addressPostalCode;

    #[ORM\Column]
    private string $addressCity;

    #[ORM\Column]
    private string $addressCountry;

    #[ORM\OneToOne(mappedBy: 'address', targetEntity: Company::class)]
    private Company $company;

    #[ORM\OneToOne(mappedBy: 'address', targetEntity: Restaurant::class)]
    private Restaurant $restaurant;

    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'addresses')]
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddressLine(): string
    {
        return $this->addressLine;
    }

    public function setAddressLine(string $addressLine): Address
    {
        $this->addressLine = $addressLine;

        return $this;
    }

    public function getAddressLine2(): ?string
    {
        return $this->addressLine2;
    }

    public function setAddressLine2(?string $addressLine2): Address
    {
        $this->addressLine2 = $addressLine2;

        return $this;
    }

    public function getAddressLine3(): ?string
    {
        return $this->addressLine3;
    }

    public function setAddressLine3(?string $addressLine3): Address
    {
        $this->addressLine3 = $addressLine3;

        return $this;
    }

    public function getAddressPostalCode(): string
    {
        return $this->addressPostalCode;
    }

    public function setAddressPostalCode(string $addressPostalCode): Address
    {
        $this->addressPostalCode = $addressPostalCode;

        return $this;
    }

    public function getAddressCity(): string
    {
        return $this->addressCity;
    }

    public function setAddressCity(string $addressCity): Address
    {
        $this->addressCity = $addressCity;

        return $this;
    }

    public function getAddressCountry(): string
    {
        return $this->addressCountry;
    }

    public function setAddressCountry(string $addressCountry): Address
    {
        $this->addressCountry = $addressCountry;

        return $this;
    }

    public function getCompany(): Company
    {
        return $this->company;
    }

    public function setCompany(Company $company): Address
    {
        $this->company = $company;

        return $this;
    }

    public function getRestaurant(): Restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(Restaurant $restaurant): Address
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function setUsers(ArrayCollection $users): Address
    {
        $this->users = $users;

        return $this;
    }
}
