<?php

namespace App\DataFixtures;

use App\Entity\Company;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CompanyFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        for ($i = 0; $i < 20; ++$i) {
            $company = new Company();
            $company->setName($faker->company);
            $company->setCompanyType($faker->domainName);
            $company->setCode($faker->currencyCode);
            $company->setSiret(($faker)->buildingNumber);
            $company->setAddress($this->getReference(sprintf('%s%s', 'address', $i)));

            $manager->persist($company);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            AddressFixtures::class,
        ];
    }
}
