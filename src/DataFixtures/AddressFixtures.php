<?php

namespace App\DataFixtures;

use App\Entity\Address;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AddressFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        for ($i = 0; $i < 40; ++$i) {
            $address = new Address();
            $address->setAddressLine($faker->streetAddress);
            $address->setCity($faker->city);
            $address->setCountry('France');
            $address->setPostalCode($faker->postcode);
            $this->setReference(sprintf('%s%s', 'address', $i), $address);

            $manager->persist($address);
        }

        $manager->flush();
    }
}
