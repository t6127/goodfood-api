<?php

namespace App\DataFixtures;

use App\Entity\Restaurant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class RestaurantFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        for ($i = 0; $i < 20; ++$i) {
            $restaurant = new Restaurant();
            $restaurant->setName($faker->company);
            $restaurant->setIsActive($faker->boolean());
            $restaurant->setPhone($faker->phoneNumber);
            $restaurant->setAddress($this->getReference(sprintf('%s%s', 'address', $i)));
            $manager->persist($restaurant);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            AddressFixtures::class,
        ];
    }
}
