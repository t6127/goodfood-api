start:
	docker-compose up -d
	docker exec -itu www-data goodfood_api composer install
	docker exec -itu www-data goodfood_api composer dump
	docker exec -itu www-data goodfood_api bin/console doctrine:database:drop --if-exists --force
	docker exec -itu www-data goodfood_api bin/console doctrine:database:create --if-not-exists
	docker exec -itu www-data goodfood_api bin/console doctrine:schema:update --dump-sql --force
	docker exec -itu www-data goodfood_api bin/console doctrine:fixtures:load -n

php:
	docker exec -itu www-data goodfood_api bash

cc:
	docker exec -itu www-data goodfood_api bin/console cache:clear

db-create:
	docker exec -itu www-data goodfood_api bin/console doctrine:database:create --if-not-exists

db-update:
	docker exec -itu www-data goodfood_api bin/console doctrine:schema:update --dump-sql --force

db-reset:
	docker exec -itu www-data goodfood_api bin/console doctrine:database:drop --if-exists --force
	docker exec -itu www-data goodfood_api bin/console doctrine:database:create --if-not-exists
	docker exec -itu www-data goodfood_api bin/console doctrine:schema:update --dump-sql --force
	docker exec -itu www-data goodfood_api bin/console doctrine:fixtures:load -n

up:
	docker-compose up

down:
	docker-compose down

restart:
	docker-compose down && docker-compose up

# Fix code style
cs-fix:
	docker exec -itu www-data goodfood_api php vendor/bin/php-cs-fixer fix --allow-risky=yes
