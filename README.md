# GOODFOOD-API

## Move to docker folder
```
cd docker
```

## Run the app
```
docker-compose up -d
```

## Manual installation (with docker)
###Move to docker/php
```
cd docker/php
```

###Install the environment and run the app
On linux/OS machin :
```
docker-compose up -d
docker exec -itu www-data goodfood_api composer install
docker exec -itu www-data goodfood_api composer dump
```

On Windows machin
```
docker-compose up -d
winpty docker exec -itu www-data goodfood_api composer install
winpty docker exec -itu www-data goodfood_api composer dump
```

###Create and implement database
On linux/OS machin :
```
docker exec -itu www-data goodfood_api php bin/console doctrine:database:drop --if-exists --force
docker exec -itu www-data goodfood_api php bin/console doctrine:database:create --if-not-exists
docker exec -itu www-data goodfood_api php bin/console doctrine:schema:update --force
docker exec -itu www-data goodfood_api php bin/console doctrine:fixtures:load
```

On Windows machin
```
winpty docker exec -itu www-data goodfood_api php bin/console doctrine:database:drop --if-exists --force
winpty docker exec -itu www-data goodfood_api php bin/console doctrine:database:create --if-not-exists
winpty docker exec -itu www-data goodfood_api php bin/console doctrine:schema:update --force
winpty docker exec -itu www-data goodfood_api php bin/console doctrine:fixtures:load
```